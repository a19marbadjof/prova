package cat.inspedralbes.m8.uf1.prueba.reclyer;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;
import java.util.Random;

public class Adaptador extends RecyclerView.Adapter<Adaptador.MyViewHolder> {

    static List<Integer> numeros;

    public Adaptador(List<Integer> nums){
        this.numeros=nums;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private final TextView tv;

        public MyViewHolder(View view){
            super(view);

            tv = (TextView) view.findViewById(R.id.textview_filanumeros_numeros);

            tv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    numeros.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    return true;
                }
            });
        }

        public TextView getTextView(){
            return tv;
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fila_numeros, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position){

        int dada = numeros.get(position);
        holder.getTextView().setText(""+dada);
    }

    @Override
    public int getItemCount(){
        return numeros.size();
    }

}

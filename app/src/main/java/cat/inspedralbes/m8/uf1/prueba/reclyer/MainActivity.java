package cat.inspedralbes.m8.uf1.prueba.reclyer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private List<Integer> nums;
    private Adaptador adap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = findViewById(R.id.recyclerview_main);
        rv.setLayoutManager(new LinearLayoutManager(this));

        nums = new ArrayList<>();
        for (int i=0;i<=50;i++){
            nums.add(i);
        }

        adap = new Adaptador(nums);
        rv.setAdapter(adap);
    }



    public void onclick(View view){

        final int min = 51;
        final int max = 1000;
        final int random = new Random().nextInt((max - min) + 1) + min;

        nums.add(random);
        adap.notifyDataSetChanged();
    }

}